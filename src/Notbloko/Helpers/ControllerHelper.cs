﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Notbloko.Helpers
{
    public class ControllerHelper
    {
        public static Error GetAccountId(Controller controller, out int id)
        {
            Error error = Error.OK;
            int claimId = 0;

            ClaimsIdentity identity = controller.User.Identities.FirstOrDefault(u => u.IsAuthenticated);
            if (identity != null)
            {
                Claim claim = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);
                if (!Int32.TryParse(claim == null ? "0" : claim.Value, out claimId))
                {
                    error = Error.NotAuthorized;
                }
            }
            else
            {
                error = Error.NotAuthorized;
            }

            id = claimId;

            return error;
        }
    }
}
