﻿namespace Notbloko.Helpers
{
    public enum Error
    {
        OK = 0,
        NotAuthorized,
        ExternalLoginFailure,
        FileNotFound,
        AlreadyExist,
        WrongFormat,
        NotFound,
        Unknown
    }

    public class ErrorHelper
    {
        public static string getMessage(Error error)
        {
            string result = "";

            switch (error)
            {
                case Error.OK:
                    result = "OK";
                    break;

                case Error.NotAuthorized:
                    result = "Not authorized";
                    break;

                case Error.ExternalLoginFailure:
                    result = "External login failed";
                    break;

                case Error.FileNotFound:
                    result = "File not found";
                    break;

                case Error.AlreadyExist:
                    result = "Already exist";
                    break;

                case Error.WrongFormat:
                    result = "Wrong format";
                    break;

                case Error.NotFound:
                    result = "Not found";
                    break;

                case Error.Unknown:
                    result = "Unknown";
                    break;
            }
            return result;
        }
    }
}