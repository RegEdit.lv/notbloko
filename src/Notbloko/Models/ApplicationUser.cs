﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Notbloko.Models
{
    public class ApplicationUser : IdentityUser
    {
    }
}
