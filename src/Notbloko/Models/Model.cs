﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Notbloko.Models
{
    public partial class Account
    {
        [Key]
        public int Id { get; set; }
        public string FbId { get; set; }
    }

    public partial class BankFilter
    {
        [Key]
        public int Id { get; set; }
        public int AccountId { get; set; }
        public string Name { get; set; }
        public string Expression { get; set; }
    }

    public partial class BankRecord
    {
        [Key]
        public int Id { get; set; }
        public int AccountId { get; set; }
        public DateTime Date { get; set; }
        public string Raw { get; set; }
        public int Crc32 { get; set; }
    }

}
