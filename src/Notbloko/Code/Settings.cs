﻿using Microsoft.Extensions.Configuration;

namespace Notbloko.Code
{
    public class Settings
    {
        public class _Db
        {
            public string ConnectionString;
        }
        
        public static _Db Db = new _Db();

        public static void init(IConfiguration configuration)
        {
            Db.ConnectionString = configuration.GetConnectionString("DefaultConnection");
        }
    }
}
