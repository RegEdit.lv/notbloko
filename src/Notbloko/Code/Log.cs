﻿using System;

namespace Notbloko.Code
{
    public class Log
    {
        public static string Exception(Exception ex)
        {
            string s = ex.Message;
            if (ex.InnerException != null)
            {
                s += " Inner Exception: " + ex.InnerException.Message;
            }
            return s;
        }

        public static void Error(string text)
        {
            Console.WriteLine("Error: " + text);
        }

        public static void Error(Exception ex)
        {
            Debug(Log.Exception(ex));
        }

        public static void Debug(string text)
        {
            Console.WriteLine("Debug: " + text);
        }

        public static void Debug(Exception ex)
        {
            Debug(Log.Exception(ex));
        }
    }
}