﻿function prepareUi(uiMode) {
    if (uiMode == bank.UIMode.Upload) {
        $("#navTabUpload").addClass("active");
        $("#navTabFilters").removeClass("active");
        $("#navTabReports").removeClass("active");
    } else if (uiMode == bank.UIMode.Filters) {
        $("#navTabFilters").addClass("active");
        $("#navTabUpload").removeClass("active");
        $("#navTabReports").removeClass("active");
    } else if (uiMode == bank.UIMode.Reports) {
        $("#navTabReports").addClass("active");
        $("#navTabFilters").removeClass("active");
        $("#navTabUpload").removeClass("active");
    }

    $(".datepicker").datepicker();
}

(function () {
    var app = angular.module('notblokoApp', ['ngRoute']);

    app.config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider.
                when('/upload', {
                    templateUrl: 'upload.htm',
                    controller: 'UploadController'
                }).
                when('/filters', {
                    templateUrl: 'filters.htm',
                    controller: 'FilterController'
                }).
                when('/reports', {
                    templateUrl: 'reports.htm',
                    controller: 'ReportController'
                }).
                otherwise({
                    redirectTo: '/upload'
                });
        }]);

    app.directive('customOnChange', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                //var onChangeHandler = scope.$eval(attrs.customOnChange);
                var onChangeHandler = scope.$eval(attrs.customOnChange);
                element.bind('change', onChangeHandler);
            }
        };
    });

    app.controller('UploadController', function ($scope, $http, $location, filterService, recordService) {
        $scope.uiMode = bank.UIMode.Upload;
        prepareUi($scope.uiMode);

        filterService.refresh(function () {
            $scope.filters = filterService.getList();
        });

        $scope.selectCsvFile = function () {
            $("#file").click();
        };

        $scope.uploadCsvFileNameChaged = function (event) {
            readFile(event, function (result) {
                var records = bank.parseCsvFile(result);
                console.log("parsed " + records.length + " records from file");
                
                var filters = filterService.getList(true);

                for (var i = 0; i < records.length; i++) {
                    console.debug("records: " + i + " : " + JSON.stringify(records[i]));
                    for (var j = 0; j < filters.length; j++) {
                        if (records[i].checkFilter(filters[j])) {
                            records[i].filters.push(filters[j]);
                        }
                    }
                }
                $scope.records = records;
                $scope.$apply();
            });
        };

        $scope.saveRecords = function () {
            recordService.add($scope.records, function (result) {
                alert("result " + JSON.stringify(result));
            });
        };
    });

    app.controller('FilterController', function ($scope, filterService) {
        $scope.uiMode = bank.UIMode.Filters;
        prepareUi($scope.uiMode);

        $scope.refreshFilters = function () {
            filterService.refresh(function () {
                $scope.filters = filterService.getList();
            });
        };

        $scope.addFilter = function () {
            filterService.add($scope.newFilterName, $scope.newFilterExpression, function (filter) {
                if (filter != null) {
                    $scope.filters = filterService.getList();
                }
            });
        };

        $scope.editFilter = function (id) {
            var i = helpers.array.indexOf($scope.filters, function (e) { return e.id == id; });
            $scope.filters[i].isEditMode = true;
        };

        $scope.editFilterSave = function (id) {
            var i = filterService.indexOf(id);
            if (i != -1) {
                filterService.edit(id, $scope.filters[i].name, $scope.filters[i].expression, function (filter) {
                    if (filter != null) {
                        $scope.filters = filterService.getList();
                    }
                });
            }
        };

        $scope.editFilterCancel = function (id) {
            var i = helpers.array.indexOf($scope.filters, function (e) { return e.id == id; });
            $scope.filters[i].isEditMode = false;
        };

        $scope.deleteFilter = function (id) {
            filterService.remove(id, function (filter) {
                if (filter != null) {
                    $scope.filters = filterService.getList();
                }
            });
        };
    });

    app.controller('ReportController', function ($scope, filterService, recordService) {
        $scope.uiMode = bank.UIMode.Reports;
        prepareUi($scope.uiMode);

        $scope.totalCreditNonFilter = 0;
        $scope.totalDebitNonFilter = 0;
        $scope.checkedNonFilter = false;

        filterService.refresh(function () {
            $scope.filters = filterService.getList();
        });

        $scope.filterIsActive = function (value, index, array) {
            var result = false;
            //console.log(JSON.stringify(value));

            for (var i = 0; i < value.filters.length; i++) {
                if (value.filters[i]._checked) {
                    result = true;
                    break;
                }
            }

            if ($scope.checkedNonFilter && value.filters.length == 0) {
                result = true;
            }

            console.log(value.raw + " " + result);
            return result;
        }

        $scope.requestRecords = function (dateFrom, dateTo) {
            recordService.get(dateFrom, dateTo, function (records) {
                var filters = filterService.getList(true);
                
                for (var i = 0; i < filters.length; i++) {
                    filters[i]._totalDebit = 0;
                    filters[i]._totalCredit = 0;
                    filters[i]._checked = false;
                }

                for (var i = 0; i < records.length; i++) {
                    var filtered = false;

                    for (var j = 0; j < filters.length; j++) {
                        if (records[i].checkFilter(filters[j])) {
                            records[i].filters.push(filters[j]);
                            filtered = true;

                            if (records[i].isDebit) {
                                filters[j]._totalDebit += records[i].amount;
                            } else if (records[i].isCredit) {
                                filters[j]._totalCredit += records[i].amount;
                            }
                        }
                    }

                    if (!filtered) {
                        if (records[i].isCredit) {
                            $scope.totalCreditNonFilter += records[i].amount;
                        } else if (records[i].isDebit) {
                            $scope.totalDebitNonFilter += records[i].amount;
                        }
                    }
                }
                console.log("received " + records.length + " records from server");

                console.log(JSON.stringify(filters));
                $scope.records = records;
                $scope.filters = filters;
            });
        };
    });
})();

function readFile(evt, callback) {
    var files = evt.target.files;
    var file = files[0];
    var reader = new FileReader();
    reader.onload = function () {
        callback(this.result);
    }
    reader.onerror = function () {
        callback(this.result);
    }
    reader.readAsText(file)
}