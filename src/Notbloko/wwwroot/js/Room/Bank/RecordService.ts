﻿
(function () {
    var app: ng.IModule = angular.module('notblokoApp');

    app.service('recordService', function ($http: angular.IHttpService) {
        var add = function (records: bank.Record[], callback) {
            bank.AddRecords($http, records, function (error: common.Error): void {
                if (error != common.Error.Ok) {
                    console.error("failed to add bank filter. Error " + error);
                    callback(error);
                } else {
                    callback(error);
                }
            });
        };

        var remove = function (id, callback) {

        };

        var get = function (dateFrom, dateTo, callback) {
            bank.GetRecords($http, dateFrom, dateTo, function (records: bank.Record[]) {
                for (var i = 0; i < records.length; i++) {
                    records[i] = bank.parseRecord(records[i].raw);
                }
                callback(records);
            });
        }

        return {
            add: add,
            remove: remove,
            get: get
        };
    });

})();
