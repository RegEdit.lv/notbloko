(function () {
    var app = angular.module('notblokoApp');
    app.service('recordService', function ($http) {
        var add = function (records, callback) {
            bank.AddRecords($http, records, function (error) {
                if (error != common.Error.Ok) {
                    console.error("failed to add bank filter. Error " + error);
                    callback(error);
                }
                else {
                    callback(error);
                }
            });
        };
        var remove = function (id, callback) {
        };
        var get = function (dateFrom, dateTo, callback) {
            bank.GetRecords($http, dateFrom, dateTo, function (records) {
                for (var i = 0; i < records.length; i++) {
                    records[i] = bank.parseRecord(records[i].raw);
                }
                callback(records);
            });
        };
        return {
            add: add,
            remove: remove,
            get: get
        };
    });
})();
