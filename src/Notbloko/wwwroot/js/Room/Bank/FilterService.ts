﻿
(function () {
    var app: ng.IModule = angular.module('notblokoApp');

    app.service('filterService', function ($http) {
        var list = [];

        var refresh = function (callback) {
            bank.GetFilters($http, function (response) {
                if (response == null) {
                    console.error("failed http request to get bank filters");
                } else if (response.error != 0) {
                    console.error("failed to get bank filters. Error " + response.error);
                } else {
                    list = response.filters;
                    for (var i = 0; i < list.length; i++) {
                        list[i] = new bank.Filter().setFromJSON(list[i]);
                    }
                }
                if (typeof callback === "function") {
                    callback();
                }
            });
        };

        var add = function (name, expression, callback) {
            bank.AddFilter($http, name, expression, function (response) {
                if (response == null) {
                    console.error("failed http request to add bank filter");
                    callback(null);
                } else if (response.error != 0) {
                    console.error("failed to add bank filter. Error " + response.error);
                    callback(null);
                } else {
                    list.push(response.filter);
                    callback(response.filter);
                }
            });
        };

        var edit = function (id, name, expression, callback) {
            bank.EditFilter($http, id, name, expression, function (response) {
                if (response == null) {
                    console.error("failed http request to edit bank filter");
                    callback(null);
                } else if (response.error != 0) {
                    console.error("failed to edit bank filter. Error " + response.error);
                    callback(null);
                } else {
                    var i = indexOf(id);
                    if (i != -1) {
                        list[i] = response.filter;
                    }
                    callback(response.filter);
                }
            });
        };

        var remove = function (id, callback) {
            bank.DeleteFilter($http, id, function (response) {
                if (response == null) {
                    console.error("failed http request to delete bank filter");
                    callback(null);
                } else if (response.error != 0) {
                    console.error("failed to delete bank filter. Error " + response.error);
                    callback(null);
                } else {
                    var i = indexOf(id);
                    if (id != -1) {
                        list.splice(i, 1);
                    }
                    callback();
                }
            });
        };


        var get = function (id) {
            var filter = null;

            for (var i = 0; i < list.length; i++) {
                if (list[i].id == id) {
                    filter = list[i];
                    break;
                }
            }

            return filter;
        }

        var indexOf = function (id) {
            var result = -1;

            for (var i = 0; i < list.length; i++) {
                if (list[i].id == id) {
                    result = i;
                    break;
                }
            }

            return result;
        }

        var getList = function () {
            return list;
        };

        return {
            add: add,
            edit: edit,
            remove: remove,
            getList: getList,
            refresh: refresh,
            indexOf: indexOf
        };
    });

})();
