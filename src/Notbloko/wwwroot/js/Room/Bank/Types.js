var bank;
(function (bank) {
    (function (UIMode) {
        UIMode[UIMode["None"] = 0] = "None";
        UIMode[UIMode["Upload"] = 1] = "Upload";
        UIMode[UIMode["Filters"] = 2] = "Filters";
        UIMode[UIMode["Reports"] = 3] = "Reports";
    })(bank.UIMode || (bank.UIMode = {}));
    var UIMode = bank.UIMode;
    var Filter = (function () {
        function Filter() {
            this._totalDebit = 0;
            this._totalCredit = 0;
            this._checked = false;
        }
        Filter.prototype.setFromJSON = function (filter) {
            this.id = filter.id;
            this.accountId = filter.accountId;
            this.name = filter.name;
            this.expression = filter.expression;
            return this;
        };
        Filter.prototype.check = function (s) {
            var r = s.match(this.expression);
            return r != null;
        };
        return Filter;
    }());
    bank.Filter = Filter;
    var Record = (function () {
        function Record() {
            this.id = 0;
            this.isCredit = false;
            this.isDebit = false;
            this.raw = "";
            this.filters = [];
        }
        Record.prototype.setFromJSON = function (record) {
            this.id = record.id;
            this.account = record.account;
            this.receiver = record.receiver;
            this.date = record.date;
            this.description = record.description;
            this.amount = record.amount;
            this.currency = record.currency;
            this.isDebit = record.isDebit;
            this.isCredit = record.isCredit;
            this.operationType = record.operationType;
            return this;
        };
        Record.prototype.checkFilter = function (filter) {
            return filter.check(this.raw);
        };
        return Record;
    }());
    bank.Record = Record;
    function readSwedbankFields(raw) {
        var fields = [];
        var i = 0;
        while (i < raw.length) {
            var startIndex = -1;
            var endIndex = -1;
            // search for starting "
            while (i < raw.length && raw[i] != '"') {
                i++;
            }
            if (i >= raw.length) {
                // not found
                break;
            }
            startIndex = i;
            i++;
            // search for closing ";
            while (i < raw.length && (raw[i - 1] != '"' || raw[i] != ";")) {
                i++;
            }
            if (i >= raw.length) {
                // not found
                break;
            }
            endIndex = i;
            i++;
            var field = raw.substring(startIndex, endIndex);
            fields.push(field);
        }
        return fields;
    }
    function parseRecord(raw) {
        var r = new Record();
        var isError = false;
        r.raw = raw;
        console.debug("parsing " + raw);
        var fields = readSwedbankFields(raw);
        for (var i = 0; i < fields.length; i++) {
            // ignore "..."
            fields[i] = fields[i].substr(1, fields[i].length - 2);
        }
        if (fields.length != 12) {
            console.error("Fields length " + fields.length + " expected 12");
            isError = true;
        }
        if (!isError) {
            r.account = fields[0];
        }
        if (!isError) {
            var text = fields[2];
            var n = text.split(".");
            if (n.length != 3) {
                console.error("Wrong date (" + fields[2] + ") format");
                isError = true;
            }
            var y = Number(n[2]);
            var m = Number(n[1]);
            var d = Number(n[0]);
            var now = new Date();
            if (isNaN(y) || y < 1970 || y > now.getFullYear()) {
                console.error("Wrong year " + y);
                isError = true;
            }
            if (isNaN(m) || m < 1 || m > 12) {
                console.error("Wrong month " + m);
                isError = true;
            }
            if (isNaN(d) || d < 1 || d > 31) {
                console.error("Wrong day " + d);
                isError = true;
            }
            if (!isError) {
                r.date = new Date();
                r.date.setUTCFullYear(y, m - 1, d);
                r.date.setUTCHours(0);
                r.date.setUTCMinutes(0);
                r.date.setUTCSeconds(0);
                r.date.setUTCMilliseconds(0);
            }
        }
        if (!isError) {
            r.receiver = fields[3];
            r.description = fields[4];
            r.amount = Number(fields[5].replace(',', '.'));
            if (isNaN(r.amount)) {
                console.error("Failed to convert " + fields[5] + " to number");
                isError = true;
            }
        }
        if (!isError) {
            r.currency = fields[6];
            if (fields[7] == "K") {
                r.isCredit = true;
                r.isDebit = false;
            }
            else if (fields[7] == "D") {
                r.isCredit = false;
                r.isDebit = true;
            }
            else {
                console.error("Unknown Debit/Credit flag (" + fields[7] + ")");
                isError = true;
            }
        }
        if (!isError) {
            r.operationType = fields[9];
        }
        return isError ? null : r;
    }
    bank.parseRecord = parseRecord;
    function parseCsvFile(file) {
        var records = [];
        var rows = file.split("\n");
        // ignore first 2 and last 3 row
        for (var i = 2; i < rows.length - 3; i++) {
            var r = parseRecord(rows[i]);
            records.push(r);
        }
        return records;
    }
    bank.parseCsvFile = parseCsvFile;
})(bank || (bank = {}));
