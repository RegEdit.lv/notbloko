﻿namespace bank {
    export function GetFilters(httpService, callback) {
        httpService.get("/Bank/GetFilters")
            .then(function (response) {
                callback(response.data);
            }, function (response) {
                console.error("/Bank/GetFilters request failed");
                callback(null);
            });
    }
    export function AddFilter(httpService, name, expression, callback) {
        var data = {
            name: name,
            expression: expression
        };
        httpService.post("/Bank/AddFilter", data)
            .then(function (response) {
                callback(response.data);
            }, function (response) {
                console.error("/Bank/AddFilter request failed");
                callback(null);
            });
    }

    export function EditFilter(httpService, id, name, expression, callback) {
        var data = {
            id: id,
            name: name,
            expression: expression
        };

        httpService.post("/Bank/EditFilter", data)
            .then(function (response) {
                callback(response.data);
            }, function (response) {
                console.error("/Bank/EditFilter request failed");
                callback(null);
            });
    }

    export function DeleteFilter(httpService, id, callback) {
        httpService.delete("/Bank/DeleteFilter?id=" + id)
            .then(function (response) {
                callback(response.data);
            }, function (response) {
                console.error("/Bank/DeleteFilter request failed");
                callback(null);
            });
    }

    export function GetRecords(httpService: angular.IHttpService, dateFrom: Date, dateTo: Date, callback: (records: {}) => void) {
        var data = {
            dateFrom: dateFrom,
            dateTo: dateTo
        };

        httpService.get("/Bank/GetRecords", { params: data })
            .then(function (response) {
                var result: any = <any> response.data;
                callback(result.records);
            }, function (response) {
                console.error("/Bank/GetRecords request failed");
                callback(null);
            });
    }

    export function AddRecords(httpService: angular.IHttpService, records: Record[], callback: (result: common.Error) => void): void {
        httpService.post("/Bank/AddRecords", records)
            .then(function (response) {
                alert(JSON.stringify(response.data));
                //callback(response.data);
            }, function (response) {
                console.error("/Bank/AddRecords request failed");
                callback(common.Error.Unknown);
            });
    }

    export function DeleteRecord(httpService, id, callback) {
        httpService.delete("/Bank/DeleteRecord?id=" + id)
            .then(function (response) {
                callback(response.data);
            }, function (response) {
                console.error("/Bank/DeleteRecord request failed");
                callback(null);
            });
    }

}