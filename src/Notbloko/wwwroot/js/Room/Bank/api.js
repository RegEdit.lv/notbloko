var bank;
(function (bank) {
    function GetFilters(httpService, callback) {
        httpService.get("/Bank/GetFilters")
            .then(function (response) {
            callback(response.data);
        }, function (response) {
            console.error("/Bank/GetFilters request failed");
            callback(null);
        });
    }
    bank.GetFilters = GetFilters;
    function AddFilter(httpService, name, expression, callback) {
        var data = {
            name: name,
            expression: expression
        };
        httpService.post("/Bank/AddFilter", data)
            .then(function (response) {
            callback(response.data);
        }, function (response) {
            console.error("/Bank/AddFilter request failed");
            callback(null);
        });
    }
    bank.AddFilter = AddFilter;
    function EditFilter(httpService, id, name, expression, callback) {
        var data = {
            id: id,
            name: name,
            expression: expression
        };
        httpService.post("/Bank/EditFilter", data)
            .then(function (response) {
            callback(response.data);
        }, function (response) {
            console.error("/Bank/EditFilter request failed");
            callback(null);
        });
    }
    bank.EditFilter = EditFilter;
    function DeleteFilter(httpService, id, callback) {
        httpService.delete("/Bank/DeleteFilter?id=" + id)
            .then(function (response) {
            callback(response.data);
        }, function (response) {
            console.error("/Bank/DeleteFilter request failed");
            callback(null);
        });
    }
    bank.DeleteFilter = DeleteFilter;
    function GetRecords(httpService, dateFrom, dateTo, callback) {
        var data = {
            dateFrom: dateFrom,
            dateTo: dateTo
        };
        httpService.get("/Bank/GetRecords", { params: data })
            .then(function (response) {
            var result = response.data;
            callback(result.records);
        }, function (response) {
            console.error("/Bank/GetRecords request failed");
            callback(null);
        });
    }
    bank.GetRecords = GetRecords;
    function AddRecords(httpService, records, callback) {
        httpService.post("/Bank/AddRecords", records)
            .then(function (response) {
            alert(JSON.stringify(response.data));
            //callback(response.data);
        }, function (response) {
            console.error("/Bank/AddRecords request failed");
            callback(common.Error.Unknown);
        });
    }
    bank.AddRecords = AddRecords;
    function DeleteRecord(httpService, id, callback) {
        httpService.delete("/Bank/DeleteRecord?id=" + id)
            .then(function (response) {
            callback(response.data);
        }, function (response) {
            console.error("/Bank/DeleteRecord request failed");
            callback(null);
        });
    }
    bank.DeleteRecord = DeleteRecord;
})(bank || (bank = {}));
