﻿namespace bank {
    export enum UIMode {
        None,
        Upload,
        Filters,
        Reports
    }

    export class Filter {
        id: number;
        accountId: number;
        name: string;
        expression: string;

        _totalDebit: number;
        _totalCredit: number;
        _checked: boolean;

        constructor() {
            this._totalDebit = 0;
            this._totalCredit = 0;
            this._checked = false;
        }

        setFromJSON(filter: Filter): Filter {
            this.id = filter.id;
            this.accountId = filter.accountId;
            this.name = filter.name;
            this.expression = filter.expression
            return this;
        }

        check(s: string): boolean {
            var r: RegExpMatchArray = s.match(this.expression);
            return r != null;
        }

    }
    export class Record {
        id: number;
        account: string;
        receiver: string;
        date: Date;
        description: string;
        amount: number;
        currency: string;
        isDebit: boolean;
        isCredit: boolean;
        operationType: string;

        raw: string;

        filters: Filter[];

        constructor() {
            this.id = 0;
            this.isCredit = false;
            this.isDebit = false;
            this.raw = "";
            this.filters = [];
        }

        setFromJSON(record: Record): Record {
            this.id = record.id;
            this.account = record.account;
            this.receiver = record.receiver;
            this.date = record.date;
            this.description = record.description;
            this.amount = record.amount;
            this.currency = record.currency;
            this.isDebit = record.isDebit;
            this.isCredit = record.isCredit;
            this.operationType = record.operationType;
            return this;
        }

        checkFilter(filter: Filter): boolean {
            return filter.check(this.raw);
        }
    }

    function readSwedbankFields(raw: string): string[] {
        var fields: string[] = [];
        var i: number = 0;
        while (i < raw.length) {
            var startIndex: number = -1;
            var endIndex: number = -1;

            // search for starting "
            while (i < raw.length && raw[i] != '"') {
                i++;
            }

            if (i >= raw.length) {
                // not found
                break;
            }

            startIndex = i;
            i++;
            // search for closing ";
            while (i < raw.length && (raw[i - 1] != '"' || raw[i] !=";")) {
                i++;
            }

            if (i >= raw.length) {
                // not found
                break;
            }

            endIndex = i;
            i++;

            var field: string = raw.substring(startIndex, endIndex);
            fields.push(field);
        }

        return fields;
    }

    export function parseRecord(raw: string): Record {
        var r: Record = new Record();
        var isError: boolean = false;

        r.raw = raw;

        console.debug("parsing " + raw);

        var fields: string[] = readSwedbankFields(raw);

        for (let i = 0; i < fields.length; i++) {
            // ignore "..."
            fields[i] = fields[i].substr(1, fields[i].length - 2); 
        }

        if (fields.length != 12) {
            console.error("Fields length " + fields.length + " expected 12");
            isError = true;
        }

        if (!isError) {
            r.account = fields[0];
        }

        if (!isError) {
            var text: string = fields[2];
            var n: string[] = text.split(".");

            if (n.length != 3) {
                console.error("Wrong date (" + fields[2] + ") format");
                isError = true;
            }

            var y: number = Number(n[2]);
            var m: number = Number(n[1]);
            var d: number = Number(n[0]);

            var now: Date = new Date();

            if (isNaN(y) || y < 1970 || y > now.getFullYear()) {
                console.error("Wrong year " + y);
                isError = true;
            }
            if (isNaN(m) || m < 1 || m > 12) {
                console.error("Wrong month " + m);
                isError = true;
            }
            if (isNaN(d) || d < 1 || d > 31) {
                console.error("Wrong day " + d);
                isError = true;
            }

            if (!isError) {
                r.date = new Date();
                r.date.setUTCFullYear(y, m - 1, d);
                r.date.setUTCHours(0);
                r.date.setUTCMinutes(0);
                r.date.setUTCSeconds(0);
                r.date.setUTCMilliseconds(0);
            }
        }

        if (!isError) {
            r.receiver = fields[3];
            r.description = fields[4];

            r.amount = Number(fields[5].replace(',', '.'));
            if (isNaN(r.amount)) {
                console.error("Failed to convert " + fields[5] + " to number");
                isError = true;
            }
        }

        if (!isError) {
            r.currency = fields[6];

            if (fields[7] == "K") {
                r.isCredit = true;
                r.isDebit = false;
            } else if (fields[7] == "D") {
                r.isCredit = false;
                r.isDebit = true;
            } else {
                console.error("Unknown Debit/Credit flag (" + fields[7] + ")");
                isError = true;
            }
        }

        if (!isError) {
            r.operationType = fields[9];
        }

        return isError ? null : r;
    }

    export function parseCsvFile(file: string): Record[] {
        var records: Record[] = [];
        var rows: string[] = file.split("\n");

        // ignore first 2 and last 3 row
        for (let i = 2; i < rows.length - 3; i++) {
            var r = parseRecord(rows[i]);
            records.push(r);
        }

        return records;
    }
}
