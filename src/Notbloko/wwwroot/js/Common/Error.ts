﻿namespace common {
    export enum Error {
        Ok = 0,
        NotAuthorized,
        ExternalLoginFailure,
        FileNotFound,
        AlreadyExist,
        WrongFormat,
        NotFound,
        Unknown
    }
}