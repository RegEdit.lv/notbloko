var common;
(function (common) {
    (function (Error) {
        Error[Error["Ok"] = 0] = "Ok";
        Error[Error["NotAuthorized"] = 1] = "NotAuthorized";
        Error[Error["ExternalLoginFailure"] = 2] = "ExternalLoginFailure";
        Error[Error["FileNotFound"] = 3] = "FileNotFound";
        Error[Error["AlreadyExist"] = 4] = "AlreadyExist";
        Error[Error["WrongFormat"] = 5] = "WrongFormat";
        Error[Error["NotFound"] = 6] = "NotFound";
        Error[Error["Unknown"] = 7] = "Unknown";
    })(common.Error || (common.Error = {}));
    var Error = common.Error;
})(common || (common = {}));
