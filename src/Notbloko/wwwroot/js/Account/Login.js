﻿(function () {

    var app = angular.module('notblokoApp', ['ngRoute']);

    app.config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider.
                when('/Login', {
                    templateUrl: 'login.htm',
                    controller: 'LoginController'
                }).
                when('/Enter', {
                    templateUrl: 'enter.htm',
                    controller: 'EnterController'
                }).
                when('/Register', {
                    templateUrl: 'register.htm',
                    controller: 'RegisterController'
                }).
                otherwise({
                    redirectTo: '/Login'
                });
        }]);

    app.controller('LoginController', function ($scope, $http, $location) {
        Account_GetLoginStatus($http, function (status) {
            if (status.isAuthenticated) {
                $location.path("/Enter");
            }
        });
    });

    app.controller('RegisterController', function ($scope) {
        $scope.message = "This page will be used to Register";
    });

    app.controller('EnterController', function ($scope, $http, $location) {
        Account_GetLoginStatus($http, function (status) {
            if (!status.isAuthenticated) {
                $location.path("/Login");
            }
            $scope.name = status.name;
        });
    });
})();
