﻿
function Account_GetLoginStatus(httpService, callback) {
    httpService.get("/Account/GetLoginStatus")
    .then(function (response) {
        //First function handles success
        callback(response.data);
    }, function (response) {
        var result = {isAuthenticated: false};
        callback(result);
    });
}