﻿(function () {
    var app = angular.module('notblokoApp');

    app.controller('HeaderController', function ($scope, $http) {
        $scope.header = { name: "header.html", url: "/Templates/header.html" };

        Account_GetLoginStatus($http, function (status) {
            if (!status.isAuthenticated) {
                $location.path("/Login");
            }
            $scope.accountName = status.name;
        });
    });
})();
