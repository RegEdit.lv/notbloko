var helpers;
(function (helpers) {
    var array;
    (function (array_1) {
        function indexOf(array, isElement) {
            for (var i = 0; i < array.length; i++) {
                if (isElement(array[i])) {
                    return i;
                }
            }
            return -1;
        }
        array_1.indexOf = indexOf;
    })(array = helpers.array || (helpers.array = {}));
})(helpers || (helpers = {}));
