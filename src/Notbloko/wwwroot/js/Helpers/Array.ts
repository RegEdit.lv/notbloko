﻿namespace helpers {
export namespace array {

    export function indexOf(array: any[], isElement: (n: any) => boolean): number {
        for (var i: number = 0; i < array.length; i++) {
            if (isElement(array[i])) {
                return i;
            }
        }
        return -1;
    }
}
}
