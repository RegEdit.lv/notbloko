﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Notbloko.Helpers;
using System.Linq;
using System;
using Notbloko.Data;
using Notbloko.Models;
using System.Collections.Generic;
using System.Text;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Notbloko.Controllers
{
    [Authorize]
    public class BankController : Controller
    {
        NotblokoDbContext _db = new NotblokoDbContext();

        [HttpGet]
        public IActionResult GetFilters()
        {
            Error error = Error.OK;
            int accountId = 0;
            BankFilter[] filters = null;

            error = ControllerHelper.GetAccountId(this, out accountId);

            if (error == Error.OK)
            {
                filters = _db.BankFilters.Where(f => f.AccountId == accountId).ToArray();
            }

            return Json(new { error = error, filters = filters });
        }

        [HttpPost]
        public IActionResult AddFilter([FromBody] BankFilter addFilter)
        {
            Error error = Error.OK;
            int accountId = 0;
            BankFilter filter = null;
            string name = addFilter.Name;
            string expression = addFilter.Expression;

            error = ControllerHelper.GetAccountId(this, out accountId);

            if (error == Error.OK)
            {
                if (name.Length == 0 || expression.Length == 0)
                {
                    error = Error.WrongFormat;
                }
            }

            if (error == Error.OK)
            {
                filter = _db.BankFilters.FirstOrDefault(f => f.AccountId == accountId && String.Compare(name, f.Name, true) == 0);

                if (filter != null)
                {
                    error = Error.AlreadyExist;
                }
            }

            if (error == Error.OK)
            {
                filter = new BankFilter()
                {
                    AccountId = accountId,
                    Name = name,
                    Expression = expression
                };

                _db.BankFilters.Add(filter);
                _db.SaveChanges();
            }

            return Json(new { error = error, filter = filter});
        }

        [HttpPost]
        public IActionResult EditFilter([FromBody] BankFilter editFilter)
        {
            Error error = Error.OK;
            int accountId = 0;
            BankFilter filter = null;
            int id = editFilter.Id;
            string name = editFilter.Name;
            string expression = editFilter.Expression;

            error = ControllerHelper.GetAccountId(this, out accountId);

            if (error == Error.OK)
            {
                if (name == null || expression == null || 
                    name.Length == 0 || expression.Length == 0)
                {
                    error = Error.WrongFormat;
                }
            }

            if (error == Error.OK)
            {
                filter = _db.BankFilters.FirstOrDefault(f => f.AccountId == accountId && f.Id == id);

                if (filter == null)
                {
                    error = Error.NotFound;
                }
            }

            if (error == Error.OK)
            {
                filter.Name = name;
                filter.Expression = expression;
                _db.SaveChanges();
            }

            return Json(new { error = error, filter = filter });
        }

        [HttpDelete]
        public IActionResult DeleteFilter(int id)
        {
            Error error = Error.OK;
            int accountId = 0;
            BankFilter filter = null;

            error = ControllerHelper.GetAccountId(this, out accountId);

            if (error == Error.OK)
            {
                filter = _db.BankFilters.FirstOrDefault(f => f.AccountId == accountId && f.Id == id);

                if (filter == null)
                {
                    error = Error.NotFound;
                }
            }

            if (error == Error.OK)
            {
                _db.BankFilters.Remove(filter);
                _db.SaveChanges();
            }

            return Json(new { error = error });
        }

        [HttpPost]
        public IActionResult AddRecords([FromBody] BankRecord[] records)
        {
            Error error = Error.OK;
            int accountId = 0;
            BankRecord[] duplicates = null;
            List<int> crcs = new List<int>();

            error = ControllerHelper.GetAccountId(this, out accountId);

            if (error == Error.OK)
            {
                CRCTool crc32 = new CRCTool();
                crc32.Init(CRCTool.CRCCode.CRC32);
                // Calculate crc32 for all records

                foreach (BankRecord r in records)
                {
                    r.AccountId = accountId;
                    // convert to ASCII
                    //r.Raw = Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(r.Raw));
                    //r.Raw = Encoding.ASCII.GetString(Encoding.ASCII.GetBytes(r.Raw));

                    // calculate Crc32
                    byte[] rawBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(r.Raw);
                    r.Crc32 = (int)crc32.crctablefast(rawBytes);
                    crcs.Add(r.Crc32);
                }

                duplicates = (from record in _db.BankRecords
                              where record.AccountId == accountId && crcs.Contains(record.Crc32)
                              select record).ToArray();
                 
                foreach (BankRecord record in records)
                {
                    bool duplicated = false;
                    foreach (BankRecord duplicate in duplicates)
                    {
                        if (record.Crc32 == duplicate.Crc32 && record.Raw == duplicate.Raw)
                        {
                            duplicated = true;
                        }
                    }

                    if (duplicated)
                    {
                        continue;
                    }
                    _db.BankRecords.Add(record);
                }
            }

            if (error == Error.OK)
            {
                _db.SaveChanges();
            }

            return Json(new { error = error });
        }

        [HttpGet]
        public IActionResult GetRecords(DateTime dateFrom, DateTime dateTo)
        {
            Error error = Error.OK;
            int accountId = 0;
            BankRecord []records = null;

            error = ControllerHelper.GetAccountId(this, out accountId);

            if (error == Error.OK)
            {
                records = _db.BankRecords.Where(r => 
                    r.AccountId == accountId &&
                    r.Date >= dateFrom && r.Date <= dateTo).ToArray();
            }

            return Json(new { error = error, records = records });
        }
    }
}
