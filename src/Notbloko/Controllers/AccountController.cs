﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Notbloko.Code;
using Notbloko.Data;
using Notbloko.Helpers;
using Notbloko.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Notbloko.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        readonly UserManager<ApplicationUser> _userManager;
        readonly SignInManager<ApplicationUser> _signInManager;
        NotblokoDbContext _db = new NotblokoDbContext();

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager
            //,
            //NotblokoDbContext db
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            //_db = db;
        }

        [AllowAnonymous]
        public JsonResult GetLoginStatus()
        {
            ClaimsIdentity identity = User.Identities.FirstOrDefault(u => u.IsAuthenticated);
            bool isAuthenticated = false;
            string name = "";
            if (identity != null)
            {
                isAuthenticated = identity.IsAuthenticated;
                Claim claim = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name);
                name = claim == null ? "" : claim.Value;
            }
            return Json(new { IsAuthenticated = isAuthenticated, Name = name});
        }

        [AllowAnonymous]
        public async Task<IActionResult> LogOut()
        {
            await HttpContext.Authentication.SignOutAsync("Cookie");
            await _signInManager.SignOutAsync();
            return LocalRedirect("/");
        }

        [AllowAnonymous]
        public async Task<IActionResult> AccessDenied(string returnUrl = null)
        {
            // shit happens! Most probably because Facebook authorization
            // doesn't work if it was already authorized
            // logout and go to login page
            await _signInManager.SignOutAsync();
            return RedirectToLocal("/");
        }

        [HttpPost]
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> LoginFbAccount()
        {
            string returnUrl = "/Account/MyRoom.html";

            // auto login
            const string Issuer = "https://facebook.com";
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.NameIdentifier, "1", ClaimValueTypes.Integer32, Issuer));
            claims.Add(new Claim(ClaimTypes.Name, "TestUser", ClaimValueTypes.String, Issuer));
            claims.Add(new Claim("FacebookId", "TestFbId", ClaimValueTypes.Integer32, Issuer));
            ClaimsIdentity userIdentity = new ClaimsIdentity("Facebook");
            userIdentity.AddClaims(claims);
            ClaimsPrincipal userPrincipal = new ClaimsPrincipal(userIdentity);
            await HttpContext.Authentication.SignInAsync("Cookie", userPrincipal,
                new AuthenticationProperties
                {
                    ExpiresUtc = DateTime.UtcNow.AddYears(1),
                    IsPersistent = false,
                    AllowRefresh = false
                });
            return RedirectToLocal(returnUrl);

            //return ExternalLogin("Facebook", returnUrl);
        }

        public IActionResult ExternalLogin(string provider, string returnUrl)
        {            
            bool signedIn = User.Identities.Any(u => u.IsAuthenticated);
            //_signInManager.SignOutAsync();
            //_signInManager.IsSignedIn
            // Request a redirect to the external login provider.
            string redirectUrl = Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl });
            AuthenticationProperties properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            //return waHttpContext.Authentication.ChallengeAsync(provider, properties)
            
            ChallengeResult challenge = Challenge(properties, provider);
            return challenge;
        }

        public JsonResult TestJson()
        {
            bool signedIn = User.Identities.Any(u => u.IsAuthenticated);
            return Json(new { X = 123, Y = 4 });
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
        {
            Error error = Error.OK;
            string externalId = "";
            string name = "";
            string externalProvider = "";
            Account account = null;
            ClaimsPrincipal principal = null;

            if (remoteError != null)
            {
                error = Error.ExternalLoginFailure;
                Log.Debug("External login failed:" + remoteError);
                returnUrl = "/Account/ExternalLoginFailure";
            }

            if (error == Error.OK)
            {
                ExternalLoginInfo info = await _signInManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    error = Error.ExternalLoginFailure;
                    Log.Debug("Failed to get external login info");
                    returnUrl = "/Account/InfoNull";
                }
                else
                {
                    principal = info.Principal;
                    externalId = info.ProviderKey;
                    externalProvider = info.LoginProvider;
                    Claim claim = info.Principal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name);
                    if (claim != null)
                    {
                        name = claim.Value;
                    }
                }
            }

            if (error == Error.OK)
            {
                // check database for existing user
                account = _db.Accounts.FirstOrDefault(x => x.FbId == externalId);

                if (account == null)
                {
                    // add new user
                    account = new Account()
                    {
                        FbId = externalId
                    };

                    _db.Accounts.Add(account);
                    _db.SaveChanges();
                }
            }

            if (error == Error.OK)
            {
                const string Issuer = "https://facebook.com";
                List<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, account.Id.ToString(), ClaimValueTypes.Integer32, Issuer));
                claims.Add(new Claim(ClaimTypes.Name, name, ClaimValueTypes.String, Issuer));
                claims.Add(new Claim("FacebookId", account.FbId.ToString(), ClaimValueTypes.Integer32, Issuer));
                ClaimsIdentity userIdentity = new ClaimsIdentity("Facebook");
                userIdentity.AddClaims(claims);
                ClaimsPrincipal userPrincipal = new ClaimsPrincipal(userIdentity);
                await HttpContext.Authentication.SignInAsync("Cookie", userPrincipal,
                    new AuthenticationProperties
                    {
                        ExpiresUtc = DateTime.UtcNow.AddMinutes(20),
                        IsPersistent = false,
                        AllowRefresh = false
                    });
            }

            return RedirectToLocal(returnUrl);
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return LocalRedirect(returnUrl);
            }
            else
            {
                return LocalRedirect("error.html");
            }
        }
    }
}
