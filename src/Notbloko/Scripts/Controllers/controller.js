﻿(function () {
    'use strict';

    angular
        .module('notblokoapp')
        .controller('notblokoController', notblokoController);

    notblokoController.$inject = ['$scope', 'notbloko'];

    function notblokoController($scope, notbloko) {
        $scope.notbloko = notbloko.APIData();
    }
})();
