﻿using Microsoft.EntityFrameworkCore;
using Notbloko.Code;
using Notbloko.Models;

namespace Notbloko.Data
{
    public partial  class NotblokoDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(Settings.Db.ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Do not create model automatically
        }

        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<BankFilter> BankFilters { get; set; }
        public virtual DbSet<BankRecord> BankRecords { get; set; }
    }
}
